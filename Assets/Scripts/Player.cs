﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	Rigidbody rb;
	public float speed;
	public GameObject prefabProjectile;
    public Vector3 offset;
    public float yMax;
    public float yMin;
    public float xLimit;
    //int lives = 3;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		// if(Input.GetKey("w")){
		// 	rb.AddForce(new Vector3(0, speed, 0));
		// }
		// if(Input.GetKey("s")){
		// 	rb.AddForce(new Vector3(0, -speed, 0));
		// }

        Vector3 newPosition = transform.position + new Vector3
                                                            (speed * Input.GetAxis("Horizontal") * Time.deltaTime * speed, 
                                                             speed * Input.GetAxis("Vertical") * Time.deltaTime * speed,
                                                             0);



        if(newPosition.y > yMax){
            //rb.transform.position = new Vector3(rb.transform.position.x, yMax, 0);
            newPosition.y = yMax;
            //Debug.Log("Saindo por cima");
        }

        if (newPosition.y < yMin){
            newPosition.y = yMin;
            //Debug.Log("Saindo por baixo");
        }

        if (newPosition.x > xLimit) {
            newPosition.x = xLimit;
        }

        if (newPosition.x < -xLimit){
            newPosition.x = -xLimit;
        }


        transform.position = newPosition;

        //rb.AddForce(new Vector3(0, Input.GetAxis("Vertical") * Time.deltaTime * speed , 0));


		// Debug.Log("Axis: " + Input.GetAxis("Vertical"));

		if(Input.GetButtonDown("Shoot")){
			Shoot();
		}
	}

	void Shoot(){
        //Vector3.right = new Vector3(1f, 0, 0);
        GameObject projectileGO = Instantiate(prefabProjectile, transform.position + offset, Quaternion.identity);
        projectileGO.GetComponent<Project>().SetupDirection(1f);

        //Ignora a colisao entre os objetos tipo 8
        Physics.IgnoreLayerCollision(8, 8, true);
	}

    private void OnCollisionEnter(Collision col){
        if(col.other.GetComponent<Rigidbody>().tag == "Enemy"){
            Debug.Log("Colisao com o Enemy");
        }

        if (col.other.GetComponent<Rigidbody>().tag == "Projectile"){
            Debug.Log("Colisao com o Projectile");
        }

        Debug.Log("Colisao com:" + col.other.GetComponent<Rigidbody>().name.ToString());
    }

    public void Die(){
        gameObject.SetActive(false);

        GameManager.lives--;

        if(GameManager.lives <= 0){
            
            Debug.Log("Game OVER!!!!");
            GameManager.GameOver();

        } else {
            Invoke("Spawn", 3f);
        }

    }

    void Spawn(){
        gameObject.SetActive(true);

    }

}
