﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    Rigidbody rb;
    public GameObject prefabProjectile;
    public Vector3 offset;

    public float shootInterval;
    public float yLimit;
    public float speed;
    public float horizontalSpeed;

    float timer; 

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
		
	}
	
	// Update is called once per frame
	void Update () {

        //Movimentacao
        Vector3 newPosition = transform.position + new Vector3(horizontalSpeed * Time.deltaTime, speed * Time.deltaTime, 0);

        //Limete da tela
        if (newPosition.y < -yLimit){
            newPosition.y = -yLimit;

            //inverter movimento
            speed = -speed;
        }
        if (newPosition.y > yLimit){
            newPosition.y = yLimit;

            //inverter movimento
            speed = -speed;
        }

        //Tiro do Inimigo
        if(timer > shootInterval){

            if(newPosition.x < 120f){
                Shoot();
                timer = 0f;
            }
        }

        rb.transform.position = newPosition;
        timer += Time.deltaTime;
		
	}



    void Shoot(){
        
        GameObject projectileGO = Instantiate(prefabProjectile, transform.position + offset, Quaternion.identity);
        projectileGO.GetComponent<Project>().SetupDirection(1f);

        //Ignora a colisao entre os objetos tipo 8
        Physics.IgnoreLayerCollision(8, 8, true);
    }

    public void Die(){
        GameManager.AddPoints(10);
        rb.transform.position = rb.transform.position + new Vector3(120f, 0, 0);
    }

    public void Spawn()
    {
        gameObject.SetActive(true);

    }
}
