﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    static int score;
    public static int lives;
    public float timeToSpawn;
    public GameObject enemyPreab;
    public float yLimitesParaSpawn;
    public float xSpawn;
    public float hellTime;
    public float hardLimit = 1f;
    public float redutorDoTempoDeSpawn = 0.5f;

    float timer = 0f;
    float timeToHell = 0f;

    public Text scorelabel;
    public Text liveslabel;
    public GameObject coverScreen;
    public GameObject gameOverScreen;
    public Text scoreGameOver;


    void Start(){
       
        //Valores inicias, devido o restart
        lives = 3;
        score = 0;
        timeToSpawn = 5f;
        //esconder telas
        gameOverScreen.SetActive(false);
        //coverScreen.SetActive(false);

        // pausar o jogo
        Time.timeScale = 0; 

        GameManager.ShowCoverScreen();
    }

    public static void AddPoints(int addScore){
        
        score += addScore;
        Debug.Log("Score atual é: " + score);

    }

    void Update(){
        
		timeToHell += Time.deltaTime;    
		timer += Time.deltaTime;

        if (timer > timeToSpawn){

            timer = 0f;
            Spwan(); 

        }

        if(timeToHell > hellTime) {
            Debug.Log("HELL TIME!!! time to spawn: " + timeToSpawn);
            timeToHell = 0f;
            getHarder();
        }

        scorelabel.text = "Score: " + score.ToString();
        liveslabel.text = "Lives: " + lives.ToString();

    }

    void getHarder(){

        timeToSpawn -= redutorDoTempoDeSpawn;
        //Limita a min e max da variavel timeToSpawn
        timeToSpawn = Mathf.Clamp(timeToSpawn, hardLimit, 6f);

    }

    void Spwan(){
        
        Debug.Log("Spawn de Enemy");
        Instantiate(enemyPreab, 
                    new Vector3(xSpawn, Random.Range(-yLimitesParaSpawn, yLimitesParaSpawn), 0), 
                    Quaternion.identity);
    }


    public static void GameOver(){

        GameManager instanceGM =  FindObjectOfType<GameManager>();

        instanceGM.gameOverScreen.SetActive(true);
        instanceGM.scoreGameOver.text = score.ToString();
        Time.timeScale = 0.3f;
    }

    public static void ShowCoverScreen(){

        GameManager instanceGM = FindObjectOfType<GameManager>();
        instanceGM.coverScreen.SetActive(true);

    }


    public void Restart(){
        Debug.Log("Restart game");

        SceneManager.LoadScene("Cena");

    }

    public void Play(){
        Debug.Log("PLAY");

        Time.timeScale = 1f;
        coverScreen.SetActive(false);
    }

    public void Quit(){
        Application.Quit();
    }
}
