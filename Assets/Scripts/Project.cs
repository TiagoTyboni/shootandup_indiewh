﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Project : MonoBehaviour {
    
    public float speed;
    public float range;
    public GameObject explosion;

    Rigidbody rb;
    Vector3 originalPosition;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        originalPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        // Validacao do valor absoluto *ignora o sinal
        if(Mathf.Abs(transform.position.x - originalPosition.x) > range){
            Destroy(gameObject);
        }


        //rb.AddForce(new Vector3(speed, 0, 0));
        Vector3 newPosition = transform.position + new Vector3(speed * Time.deltaTime, 0, 0);

        rb.transform.position = newPosition;
		
	}

    void OnCollisionEnter(Collision col){
        Debug.Log(col.gameObject.name);


        GameObject explosionGO = Instantiate(explosion, col.transform.position, Quaternion.identity);
        Destroy(explosionGO, 2f);

        if(col.gameObject.tag == ("Enemy")){
            
            col.gameObject.GetComponent<Enemy>().Die();
                  
        } 

        if(col.gameObject.tag == ("Player")){
            
            col.gameObject.GetComponent<Player>().Die();
            
        }
        // Destruindo o projetile
        Destroy(gameObject);

    }

    public void SetupDirection (float direciton){
        speed *= direciton;
    }
}
